CREATE SEQUENCE public.id_media_seq;
CREATE SEQUENCE public.id_seq;

CREATE TABLE public.media_feed
(
    id integer NOT NULL DEFAULT nextval('id_media_seq'::regclass),
    title text COLLATE pg_catalog."default",
    link text COLLATE pg_catalog."default",
    author text COLLATE pg_catalog."default",
    date timestamp without time zone,
    id_video text COLLATE pg_catalog."default",
    CONSTRAINT id_media_primary PRIMARY KEY (id),
    CONSTRAINT title_key_media UNIQUE (title)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.media_feed
    OWNER to crypto_feed;



CREATE TABLE public.news_feed
(
    id integer NOT NULL DEFAULT nextval('id_seq'::regclass),
    title text COLLATE pg_catalog."default",
    link text COLLATE pg_catalog."default",
    categories text COLLATE pg_catalog."default",
    content text COLLATE pg_catalog."default",
    date timestamp without time zone,
    CONSTRAINT id_primary PRIMARY KEY (id),
    CONSTRAINT title_key UNIQUE (link)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.news_feed
    OWNER to crypto_feed;    