let Parser = require('rss-parser');
let parser = new Parser();
const fs = require('fs') 
const pg = require('pg');

const pool = new pg.Pool({
  user: 'crypto_feed',
  host: 'postgres',
  database: 'crypto_feed',
  password: '',
  port: 5432,
})
// CRON TO AUTOMATE THE 'node fetchRss.js' command 


async function saveInDb(entry){
  //console.log(entry)
  /*console.log('Date:' + entry.pubDate);
  console.log('Title:' + entry.title);
  console.log('Categories:' + entry.categories);
  console.log('link:' + entry.link);*/


	var into = '(date,title,link,content,categories)';
	var values = "'"+ entry.pubDate + "' ,'" + entry.title + "' , '" + entry.link + "' ,'" + entry.content + "' ,'" + entry.categories + "'";
    insert = pool.query("INSERT INTO news_feed " + into + " VALUES (" + values + ") ON CONFLICT ON CONSTRAINT title_key DO NOTHING");
    insert.then( (res, err) => {
        if(res) return true
        if(err) {
          console.log(err); 
          return false
        }
    })
}

const readFeed = (async (list_address) => {
  list_address.forEach( async address => {
    let feed = await parser.parseURL(address);
    feed.items.forEach(item => {
      saveInDb(item)
    });
  });
  return true
});

const readFile = ( async (path) => {
    var array = fs.readFileSync(path).toString().split('\n');
    return array
});

const getNews = (async () => {
    console.log('Getting rss addresses');
    const journal_addresses_feed = await readFile('./utils/journal_rss.txt');
    const feed = readFeed(journal_addresses_feed);
    return feed;
})

async function run(){
  await getNews().catch(err => console.log(err));
}

run().catch(err => console.log(err));